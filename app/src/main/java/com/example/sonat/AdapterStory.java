package com.example.sonat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterStory  extends RecyclerView.Adapter<AdapterStory.ViewHolder> {

    private Context context;
    private LayoutInflater layoutInflater;

    public AdapterStory(Context context){
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.adapter_story,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterStory.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView imgStory;

        public ViewHolder(View itemView) {
            super(itemView);

            imgStory = itemView.findViewById(R.id.imgStory);
        }
    }

}
