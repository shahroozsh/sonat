package com.example.sonat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterViewPager extends RecyclerView.Adapter<AdapterViewPager.ViewHolder> {



    private Context context;
    private LayoutInflater layoutInflater;

    public AdapterViewPager(Context context){
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.adapter_viewpager,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgViewPager;

        public ViewHolder(View itemView) {
            super(itemView);

            imgViewPager = itemView.findViewById(R.id.imgViewPager);
        }
    }



}
