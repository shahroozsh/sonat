package com.example.sonat;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.core.view.ViewCompat;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.material.navigation.NavigationView;

public class BlankFragment extends Fragment {

    private RecyclerView recyclerStory;
    private AdapterStory adapterStory;
    private ViewPager2 viewPager2;
    private AdapterViewPager adapterViewPager2;
    private NestedScrollView nestedScrollBase;
    private PlayerView playerView;
    private SimpleExoPlayer simpleExoPlayer;
    String videoURL = "https://media.geeksforgeeks.org/wp-content/uploads/20201217163353/Screenrecorder-2020-12-17-16-32-03-350.mp4";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);
//        nestedScrollBase = view.findViewById(R.id.nestedScrollBase);
//        nestedScrollBase.setFillViewport(true);

        recyclerStory = view.findViewById(R.id.recyclerStory);
        recyclerStory.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        viewPager2 = view.findViewById(R.id.viewPagerDashboardSlide);
        playerView = view.findViewById(R.id.playerView_main);
        simpleExoPlayer = new SimpleExoPlayer.Builder(getContext()).build();

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapterStory = new AdapterStory(getContext());
        recyclerStory.setAdapter(adapterStory);

        adapterViewPager2 = new AdapterViewPager(getContext());
        viewPager2.setOffscreenPageLimit(3);

        viewPager2.setAdapter(adapterViewPager2);

        float pageMargin = getResources().getDimensionPixelOffset(R.dimen.pageMargin);
        float pageOffset = getResources().getDimensionPixelOffset(R.dimen.offset);

        viewPager2.setPageTransformer(new ViewPager2.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                float myOffset = position * -(2 * pageOffset + pageMargin);
                if (viewPager2.getOrientation() == ViewPager2.ORIENTATION_HORIZONTAL) {
                    if (ViewCompat.getLayoutDirection(viewPager2) == ViewCompat.LAYOUT_DIRECTION_RTL) {
                        page.setTranslationX(-myOffset);
                    } else {
                        page.setTranslationX(myOffset);
                    }
                } else {
                    page.setTranslationY(myOffset);
                }
            }
        });
        createVideo();
    }

    public void createVideo() {
        playerView.setFocusable(false);

// Build the media item.
        MediaItem mediaItem = MediaItem.fromUri(videoURL);
// Set the media item to be played.
        simpleExoPlayer.setMediaItem(mediaItem);
// Prepare the player.
        simpleExoPlayer.prepare();
//        simpleExoPlayer.setPlayWhenReady(true);
        playerView.setPlayer(simpleExoPlayer);


        playerView.setFocusable(false);
// Start the playback.
//        simpleExoPlayer.play();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        simpleExoPlayer.release();
    }
}